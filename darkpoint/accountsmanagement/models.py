from uuid import uuid4
from datetime import datetime

from django.contrib.auth.models import User
from django.db.models import BooleanField, CharField
from django.db.models import DateTimeField, OneToOneField
from django.db.models import Model, CASCADE


class Profile(Model):
    """ Two things are added: activated (boolean) for customer email verification
        and activation id.
    """
    user = OneToOneField(User, on_delete=CASCADE)
    activated = BooleanField(default=False)
    activation_id = CharField(max_length=100, unique=True, default=uuid4)
    activation_expiry = DateTimeField()

    def __str__(self):
        return self.user.username
