from datetime import datetime, timedelta

from django.contrib.auth.models import User
from django import forms
from django.core import validators
from . import models
from django.http import JsonResponse
from django.core.mail import send_mail
from allauth.account.forms import SignupForm


class RegistrationForm(forms.Form):
    username = forms.CharField(label="", widget=forms.TextInput(
        attrs={'placeholder': 'username ...', 'class': 'form-control'}),
        max_length=30, min_length=3, validators=[validators.validate_slug])
    email = forms.EmailField(label="", widget=forms.EmailInput(
        attrs={'placeholder': 'email ...', 'class': 'form-control'}),
        max_length=100, error_messages={'invalid': ("Email invalid.")},
        validators=[validators.EmailValidator])
    password1 = forms.CharField(label="", max_length=50, min_length=6, widget=forms.PasswordInput(
        attrs={'placeholder': 'password', 'class': 'form-control'}))
    password2 = forms.CharField(label="", max_length=50, min_length=6, widget=forms.PasswordInput(
        attrs={'placeholder': 'confirm password', 'class': 'form-control'}))

    # Override clean method to check password match
    def clean(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password1 != password2:
            self._errors['password2'] = forms.utils.ErrorList(["Passwords don't match"])

        return self.cleaned_data

    # Override of save method for saving both User and Profile objects
    def save(self, datas):
        u = User.objects.create_user(datas['username'],
                                     datas['email'],
                                     datas['password1'])
        u.save()
        profile = models.Profile()
        profile.user = u
        profile.activation_expiry = datetime.now() + timedelta(days=2)
        profile.save()
        # self.send_activation_email(profile.user.username, 'http://localhost:8000/accounts/activate/')
        print('hello there')
        return profile

    # def sendEmail(self, datas):
    #     link = "http://yourdomain.com/activate/"+datas['activation_key']
    #     c = Context({'activation_link': link, 'username': datas['username']})
    #     f = open(MEDIA_ROOT+datas['email_path'], 'r')
    #     t = Template(f.read())
    #     f.close()
    #     message = t.render(c)
    #     # print unicode(message).encode('utf8')
    #     send_mail(datas['email_subject'], message,
    #               'yourdomain <no-reply@yourdomain.com>', [datas['email']], fail_silently=False)

    def send_activation_email(self, user_name, activation_link):
        """
        HELLO
        """
        try:
            user = User.objects.filter(username=user_name).first()
            if user.activated:
                return JsonResponse({"error": 'User is already activated.'})

            subject = 'DarkPoint Activation Link'
            body = ("Hi , Sharif ahmad" + " Hope you are doing fine. "
                    "To activate your account,"
                    + " please click on the link below: \n"
                    + activation_link + str(user.activation_id))
            # user.email_user(subject, body)
            print('hello there email')
            send_mail(subject, body, recipient_list=['sharifahmad2061@gmail.com'], fail_silently=False)
            return JsonResponse({"message": {"email": ["Please check email for "
                                                       "account activation."]}})
        except AttributeError:
            return JsonResponse({"error": {"username": ["User Does not exist."]}})


class CustomSignupForm(SignupForm):
    first_name = forms.CharField(label='First Name', widget=forms.TextInput(
        attrs={'placeholder': 'First Name', 'class': 'form-control', 'required': True}))
    last_name = forms.CharField(label='Last Name', widget=forms.TextInput(
        attrs={'placeholder': 'Last Name', 'class': 'form-control', 'required': True}))

    def clean(self):
        super(CustomSignupForm, self).clean()
        first_name = self.cleaned_data.get('first_name')
        last_name = self.cleaned_data.get('last_name')
        if not first_name:
            self.add_error('first_name', 'first name is required')
        if not last_name:
            self.add_error('last_name', 'last name is required')
        if first_name == last_name:
            self.add_error(None, 'first and last name cannot be same')
        return self.cleaned_data

    def save(self, request):
        user = super(CustomSignupForm, self).save(request)
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.save()
        return user
