document.addEventListener('DOMContentLoaded', () => {
	$('#signup-form').submit((event) => {
		form = document.querySelector('#signup-form')
		if (form.checkValidity() == false) {
			form.classList.add('was-validated')
			event.preventDefault()
			event.stopPropagation()
			return
		}
		// let password = $('#password').val()
		// let password_confirmation = $('#password_confirmation').val()
		// if (password != password_confirmation) {
		// 	console.log('passwords dont match')
		// 	$('#password_confirmation').addClass('is-invalid')
		// 	// $('#signup-form').addClass('was-validated')
		// 	event.preventDefault()
		// 	event.stopPropagation()
		// 	return
		// }
		// console.log($('#signup-form').serializeArray())
		fdata = $('#signup-form').serializeArray()
		$.ajax({
			url: '/accounts/register/',
			data: fdata,
			type: 'POST',
			success: (result) => {
				console.log(result)
			}
		})
		event.preventDefault()
	})
})
