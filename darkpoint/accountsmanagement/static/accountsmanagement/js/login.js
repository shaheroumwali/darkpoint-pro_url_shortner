document.addEventListener('DOMContentLoaded', () => {
	$('#login-form').submit((event) => {
		fdata = $('#login-form').serializeArray()
		// console.log(fdata)
		$.ajax({
			url: '/accounts/login/',
			type: 'POST',
			data: fdata,
			success: (result) => {
				console.log(result)
			}
		})
		event.preventDefault()
		event.stopPropagation()
	})
})
