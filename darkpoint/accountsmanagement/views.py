from datetime import timedelta

from django.shortcuts import render, redirect
from django.views import View
from django.http import JsonResponse
from django.core.mail import send_mail
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.utils import timezone

from . import forms
from .models import Profile

# Create your views here.


class UserRegistration(View):
    """
    hello
    """

    def get(self, request, *args, **kwargs):
        # form = forms.RegistrationForm()
        if not request.user.is_authenticated:
            return render(request, 'accountsmanagement/register.html')
        else:
            return redirect('/')

    def post(self, request, *args, **kwargs):
        # form = forms.RegistrationForm(request.POST)
        print(request.POST)
        if request.POST.get('first_name') != '':
            first_name = request.POST.get('first_name')
        else:
            first_name = None
            return JsonResponse({'error': 'first_name'})
        if request.POST.get('last_name') != '':
            last_name = request.POST.get('last_name')
        else:
            last_name = None
            return JsonResponse({'error': 'last_name'})
        if request.POST.get('user-name') != '':
            user_name = request.POST.get('user-name')
        else:
            user_name = None
            return JsonResponse({'error': 'user-name'})
        if request.POST.get('email') != '':
            email = request.POST.get('email')
        else:
            email = None
            return JsonResponse({'error': 'email'})
        if request.POST.get('password') != '' and len(request.POST.get('password')) >= 8:
            pasword = request.POST.get('password')
        else:
            pasword = None
            return JsonResponse({'error': 'password'})
        try:
            user = User.objects.create_user(user_name, email, pasword, first_name=first_name, last_name=last_name)
            profile = Profile()
            profile.user = user
            print(timezone.now())
            print(timezone.now() + timedelta(days=2))
            profile.activation_expiry = timezone.now() + timedelta(days=2)
            profile.save()
            subject = 'DarkPoint Activation Link'
            body = ("Hi , " + user.get_full_name() + " Hope you are doing fine. "
                    "To activate your account,"
                    + " please click on the link below: \n"
                    + 'http://localhost:8000/accounts/activate/' + str(profile.activation_id))
            # user.email_user(subject, body)
            print('hello there email')
            profile.user.email_user(subject, body, from_email='dark@point.com')
            # send_mail(subject, body, recipient_list=['sharifahmad2061@gmail.com'], from_email='dark@point.com')
            # form.send_activation_email('sharif', 'http://localhost:8000/accounts/activate/')
        except Exception as exc:
            print(exc.__doc__)
            return JsonResponse({'error': str(exc)})
        return redirect('/accounts/login/')


class UserLogin(View):
    """
    hello
    """

    def get(self, request, *args, **kwargs):
        """
        hello
        """
        if not request.user.is_authenticated:
            return render(request, 'accountsmanagement/login.html')
        else:
            return redirect('/')

    def post(self, request, *args, **kwargs):
        """
        hello
        """
        print(request.POST)
        if request.POST.get('username') != '':
            username = request.POST.get('username')
        else:
            username = None
        if len(request.POST.get('password')) >= 8:
            password = request.POST.get('password')
        else:
            password = None
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('/')
        else:
            return JsonResponse({'error': 'user or password incorrect'})


class UserLogout(View):
    """
    hello
    """

    def get(self, request, *args, **kwargs):
        logout(request)
        return redirect('/accounts/login/')
