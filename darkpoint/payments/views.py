from datetime import timedelta

from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from django.utils import timezone

from .models import Subscription

# Create your views here.


class Subscribe(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, "payments/subscribe.html")

    def post(self, request, *args, **kwargs):
        print(request.POST)
        if request.POST.get('currency') != 'USD':
            return render(request, 'payments/payment-failure.html', {'reason': 'currency is not correct'})
        elif request.POST.get('amount_paid') != '20.00':
            return render(request, 'payments/payment-failure.html', {'reason': 'amount is not paid full'})
        else:
            email = request.POST.get('payer_email')
            payer_id = request.POST.get('payer_id')
            subscription = Subscription()
            subscription.customer_id = payer_id
            subscription.customer_email = email
            subscription.expiration_date = timezone.now() + timedelta(days=30)
            subscription.user = request.user
            subscription.save()
        return JsonResponse({'redirect_to': reverse('url_shorten')})


class PaymentSuccess(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        print(*args)
        print(**kwargs)
        return render(request, 'payments/payment-success.html')


class PaymentFailure(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, 'payments/payment-failure.html')
