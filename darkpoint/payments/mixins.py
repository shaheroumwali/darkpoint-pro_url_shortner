from django.shortcuts import redirect
from django.utils import timezone
from .models import Subscription


class RequireSubscriptionMixin:
    def dispatch(self, request, *args, **kwargs):
        subscription = Subscription.objects.filter(user=request.user).first()
        if not subscription or timezone.now() > subscription.expiration_date:
            return redirect('/payments/subscribe/')
        else:
            return super(RequireSubscriptionMixin, self).dispatch(request, *args, **kwargs)
