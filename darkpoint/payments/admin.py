from django.contrib import admin
from .models import Subscription

# Register your models here.


@admin.register(Subscription)
class SubscriptionAdmin(admin.ModelAdmin):
    date_hierarchy = 'subscription_date'
    list_display = ['customer_email', 'subscription_date', 'expiration_date', 'user_email']
    list_filter = ['customer_email', 'subscription_date', 'expiration_date']
    search_fields = ['user__email', 'user__username']
