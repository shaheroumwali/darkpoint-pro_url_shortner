from django.urls import path
from .views import Subscribe, PaymentSuccess, PaymentFailure

urlpatterns = [
    path('subscribe/', Subscribe.as_view(), name='subscribe'),
    path('payment-success', PaymentSuccess.as_view(), name='payment-success'),
    path('payment-failure', PaymentFailure.as_view(), name='payment-failure')
]
