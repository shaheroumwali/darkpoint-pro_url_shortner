from django.db import models
from django.contrib.auth import get_user_model
from django.utils import timezone
from datetime import timedelta

# Create your models here.


class Subscription(models.Model):
    STATE_ACTIVE = 0
    STATE_CANCELLED = 1
    STATE_PAUSED = 2

    STATE_CHOICES = (
        (STATE_ACTIVE, 'Active'),
        (STATE_CANCELLED, 'Cancelled'),
        (STATE_PAUSED, 'Paused'),
    )

    customer_id = models.CharField(max_length=16, blank=True, null=True)
    customer_email = models.EmailField(null=True, blank=True)
    subscription_date = models.DateTimeField(default=timezone.now, blank=True)
    expiration_date = models.DateTimeField(blank=True)
    state = models.PositiveSmallIntegerField(
        choices=STATE_CHOICES, default=STATE_ACTIVE)

    user = models.OneToOneField(
        get_user_model(), on_delete=models.CASCADE, null=True, blank=True)

    @property
    def user_email(self):
        return self.user.email
