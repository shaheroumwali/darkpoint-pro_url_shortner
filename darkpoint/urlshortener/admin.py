from django.contrib import admin
from .models import ShortUrl

# admin.site.register(ShortUrl)


@admin.register(ShortUrl)
class ShortUrlAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_on'
    list_display = ['uid', 'url', 'clicks', 'created_on', 'expiry_date', 'url_active', 'admin_id']
    list_filter = ['created_on', 'expiry_date', 'url_active', 'user__email', 'user__username']
    search_fields = ['user__email', 'user__username']
