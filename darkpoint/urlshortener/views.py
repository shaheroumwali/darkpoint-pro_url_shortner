from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import F
from django.http import HttpResponse
from django.shortcuts import redirect, render, get_object_or_404
from django.views import View
from django.views.generic import ListView
from django.contrib.auth.hashers import make_password, check_password
from datetime import timedelta
from django.utils import timezone
from requests import Response


from .forms import ShortUrlForm, VerifyPasswordForm
from .models import ShortUrl
from payments.mixins import RequireSubscriptionMixin


# Create your views here.


class UrlShorten(LoginRequiredMixin, RequireSubscriptionMixin, View):
    """
    render the url shorten page and receives the url
    """

    def get(self, request, *args, **kwargs):
        form = ShortUrlForm()
        return render(request, 'urlshortener/shorten.html', {'form': form})

    def post(self, request, *args, **kwargs):
        form = ShortUrlForm(request.POST)
        # print(dir(form), form)
        if form.is_valid():
            # print(self.request.user, request.user, form.cleaned_data)
            # form.cleaned_data['user'] = request.user
            # print(form.cleaned_data)
            new_url = form.save(commit=False)
            new_url.user = request.user
            # print(new_url.url_password)
            if new_url.url_password:
                new_url.url_password = make_password(new_url.url_password)
                if new_url.expiry_date:
                    new_url.expiry_date = new_url.expiry_date + timedelta(days=1)
            new_url.save()
        else:
            return render(request, 'urlshortener/shorten.html', {'form': form})
        return redirect('/')


class UrlDelete(LoginRequiredMixin, RequireSubscriptionMixin, View):
    def get(self, request, *args, **kwargs):
        ShortUrl.objects.filter(uid=kwargs['short_url_uid']).delete()
        return redirect("/")


class UrlRedirect(View):
    """
    updates click count and redirect url
    """

    def get(self, request, *args, **kwargs):
        try:
            short_url = ShortUrl.objects.get(uid=kwargs['short_url_uid'], )
        except ShortUrl.DoesNotExist:
            return HttpResponse("Not found", status=404)

        # Update total clicks
        if short_url.url_active:
            if not short_url.url_password:
                ShortUrl.objects.filter(id=short_url.id).update(clicks=F('clicks') + 1)
                return redirect(short_url.url)
            else:
                return redirect(f'/authorize/{short_url.uid}/')
        else:
            return HttpResponse("Url Not active", status=404)


class AuthorizeUrl(View):
    def get(self, request, *args, **kwargs):
        form = VerifyPasswordForm()
        return render(request, 'urlshortener/password.html', {'form': form, 'uid': kwargs.get('short_url_uid')})

    def post(self, request, *args, **kwargs):
        form = VerifyPasswordForm(request.POST)
        if form.is_valid():
            password = form.cleaned_data.get('password')
            short_url_uid = kwargs.get('short_url_uid')
            short_url = ShortUrl.objects.get(uid=short_url_uid)
            if short_url.url_active:
                if timezone.now() < short_url.expiry_date:
                    if check_password(password, short_url.url_password):
                        return redirect(short_url.url)
                    else:
                        return HttpResponse('Password is incorrect')
                else:
                    return HttpResponse('url is expired')
            else:
                return HttpResponse('url is not active anymore')
        else:
            return render(request, 'urlshortener/password.html', {'form': form})


class AllUrls(LoginRequiredMixin, RequireSubscriptionMixin, ListView):

    model = ShortUrl
    paginate_by = 8
    template_name = 'urlshortener/allurls.html'

    def get_queryset(self):
        return ShortUrl.objects.filter(user=self.request.user)
