from django.urls import path, include
from urllib3.util import Url

from .views import UrlRedirect, UrlShorten, AllUrls, UrlDelete, AuthorizeUrl

urlpatterns = [
    path('', AllUrls.as_view(), name='all_urls'),
    path('shorten/', UrlShorten.as_view(), name='url_shorten'),
    path('<slug:short_url_uid>/', UrlRedirect.as_view(), name='url_redirect'),
    path('delete/<slug:short_url_uid>/', UrlDelete.as_view(), name='url_delete'),
    path('authorize/<slug:short_url_uid>/', AuthorizeUrl.as_view(), name='authorize_url'),

]
