import pytz

from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.contrib.auth import get_user_model
from django.conf import settings
from django.utils.crypto import get_random_string
from django.core.validators import URLValidator

import string


def generate_random_uid():
    s = get_random_string(length=6, allowed_chars=string.ascii_letters)
    return s


class ShortUrl(models.Model):

    uid = models.CharField(
        default=generate_random_uid, max_length=6, unique=True)
    url = models.CharField(max_length=1024)
    clicks = models.PositiveIntegerField(default=0)
    created_on = models.DateTimeField(default=timezone.now)
    url_active = models.BooleanField(default=True)
    url_password = models.CharField(max_length=128, blank=True, null=True)
    expiry_date = models.DateTimeField(blank=True, null=True)
    user = models.ForeignKey(
        get_user_model(), on_delete=models.CASCADE, null=True, blank=True)

    @classmethod
    def create_and_validate(cls, url_target, user):
        if not url_target.startswith('http://') and not url_target.startswith(
                'https://'):
            url_target = 'http://' + url_target

        # Throw ValidationError exception if url_target is not incorrect format
        validate = URLValidator()
        validate(url_target)

        short_url, _ = ShortUrl.objects.get_or_create(
            url=url_target,
            user=user,
        )

        return short_url

    @property
    def full_short_url_with_scheme(self):
        return f"http://darkpoint.pro/{self.uid}/"

    def save(self, *args, **kwargs):
        if not self.url.startswith('http://') and not self.url.startswith(
                'https://'):
            self.url = 'http://' + self.url
        super().save(*args, **kwargs)

    @property
    def admin_id(self):
        return f'{self.user.email}'
