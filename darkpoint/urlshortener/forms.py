from django import forms

from .models import ShortUrl


class ShortUrlForm(forms.ModelForm):
    url = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter long url'}))
    url_password = forms.CharField(
        label='Password (optional)', max_length=15, min_length=8, widget=forms.PasswordInput(
            attrs={'class': 'form-control', 'placeholder': 'Password'}), required=False)
    expiry_date = forms.DateTimeField(label='Expiration Date (optional)', widget=forms.DateInput(
        attrs={'type': 'date', 'class': 'form-control'}), required=False)

    class Meta:
        model = ShortUrl
        fields = ['url', 'url_password', 'expiry_date']

    def clean_url(self):
        recvd_url = self.cleaned_data.get('url')

        if not recvd_url.startswith('http://') and not recvd_url.startswith('https://'):
            self.add_error('url', 'url does not start with http://')
        return recvd_url


class VerifyPasswordForm(forms.Form):
    password = forms.CharField(label='Password', min_length=8, max_length=15, widget=forms.PasswordInput(
        attrs={'class': 'form-control', 'placeholder': 'Password', 'required': True}))
